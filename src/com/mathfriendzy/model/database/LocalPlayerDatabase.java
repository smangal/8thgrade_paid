package com.mathfriendzy.model.database;

import java.util.ArrayList;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mathfriendzy.database.Database;
import com.mathfriendzy.model.learningcenter.PlayerEquationLevelObj;

public class LocalPlayerDatabase 
{
	private final String USER_ID		 		= "USER_ID";
	private final String PLAYER_ID	 			= "PLAYER_ID";
	private final String EQUATION_TYPE	 		= "EQUATION_TYPE";
	private final String LEVEL	 				= "LEVEL";
	private final String STARS	 				= "STARS";
	
	private final String LOCAL_PLAYER_TBL 		= "LocalPlayerTable";

	private final String CREATE_TABLE	 	= "CREATE TABLE "+LOCAL_PLAYER_TBL+"("+USER_ID+" INTEGER, "
			+PLAYER_ID+" INTEGER, "+EQUATION_TYPE+" INTEGER, "+LEVEL+" INTEGER," + STARS+" INTEGER)";

	private final String TAG  					= this.getClass().getSimpleName();

	private SQLiteDatabase dbConn = null;

	/**
	 * use to create table Top100 by checking if it exist or not
	 * @param context
	 */
	public void createLocalPlayerTable(Context context)
	{
		this.openConn(context); 
		Cursor cursor = dbConn.rawQuery("SELECT * FROM sqlite_master WHERE type = ? AND name = ?", 
				new String[] {"table", LOCAL_PLAYER_TBL});
		if(cursor.moveToNext())
		{  
			cursor.close();
		}
		else
		{	if(cursor != null)
				cursor.close();
			dbConn.execSQL(CREATE_TABLE);
		}
				
		this.closeConn();
	}
	
	/**
	 * This method open the connection with the database
	 * @param context
	 */
	public void openConn(Context context)
	{
		Database database = new Database(context);
		database.open();
		dbConn = database.getConnection();
	}

	/**
	 * This method close the connection with the database
	 */
	public void closeConn()
	{
		if(dbConn != null)
			dbConn.close();
	}
	
	
	/**
	 * This method insert the data into the player Equation level table
	 * @param playrEquationObj
	 */
	public void insertIntoPlayerEquationLevel(PlayerEquationLevelObj playrEquationObj)
	{
		ContentValues contentValues = new ContentValues();
		contentValues.put("USER_ID", playrEquationObj.getUserId());
		contentValues.put("PLAYER_ID", playrEquationObj.getPlayerId());
		contentValues.put("EQUATION_TYPE", playrEquationObj.getEquationType());
		contentValues.put("LEVEL", playrEquationObj.getLevel());
		contentValues.put("STARS", playrEquationObj.getStars());	

		//Log.e("", "PLAYER_ID : PlayerEquationLevel : "+playrEquationObj.getPlayerId()+"  level "+playrEquationObj.getLevel());
		dbConn.insert(LOCAL_PLAYER_TBL, null, contentValues);
	}
	
	
	/**
	 * This method return the player level data
	 * @param playerId
	 * @return
	 */
	public ArrayList<PlayerEquationLevelObj> getPlayerEquationLevelDataByPlayerId(String playerId)
	{
		ArrayList<PlayerEquationLevelObj> playerDataList = new ArrayList<PlayerEquationLevelObj>();

		String query = "select * from " + LOCAL_PLAYER_TBL + " where PLAYER_ID = '" + playerId + "'";
		//Log.e("", "query "+query);
		Cursor cursor = dbConn.rawQuery(query, null);

		while(cursor.moveToNext())
		{
			PlayerEquationLevelObj playerObj = new PlayerEquationLevelObj();
			playerObj.setUserId(cursor.getString(cursor.getColumnIndex("USER_ID")));
			playerObj.setPlayerId(cursor.getString(cursor.getColumnIndex("PLAYER_ID")));
			playerObj.setEquationType(cursor.getInt(cursor.getColumnIndex("EQUATION_TYPE")));
			playerObj.setLevel(cursor.getInt(cursor.getColumnIndex("LEVEL")));
			playerObj.setStars(cursor.getInt(cursor.getColumnIndex("STARS")));				
			playerDataList.add(playerObj);	

		}

		if(cursor != null)
			cursor.close();
		return playerDataList;
	}
	
	/**
	 * This method delete the record from LOCAL_PLAYER_TBL
	 * @param playerId
	 */
	public void deleteFromLocalTable()
	{
		dbConn.delete(LOCAL_PLAYER_TBL , null , null);
	}
}
