package com.mathfriendzy.model.learningcenter;

public class LearningCenterTransferObj 
{
	
	private int equationId;
	private String question;	
	private int operationId;
	
	private String answer;
	private String option1;
	private String option2;
	private String option3;
	private String  LearningCenterOperation;
	private int operationBackgroundImg;

	
	public int getOperationId() {
		return operationId;
	}

	public void setOperationId(int operationId) {
		this.operationId = operationId;
	}

	public int getEquationId() {
		return equationId;
	}

	public void setEquationId(int equationId) {
		this.equationId = equationId;
	}

	public String getQuestion() {
		return question;
	}

	public void setQuestion(String question) {
		this.question = question;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}

	public String getOption1() {
		return option1;
	}

	public void setOption1(String option1) {
		this.option1 = option1;
	}

	public String getOption2() {
		return option2;
	}

	public void setOption2(String option2) {
		this.option2 = option2;
	}

	public String getOption3() {
		return option3;
	}

	public void setOption3(String option3) {
		this.option3 = option3;
	}
	public String getLearningCenterOperation() {
		return LearningCenterOperation;
	}

	public void setLearningCenterOperation(String learningCenterOperation) {
		LearningCenterOperation = learningCenterOperation;
	}


	public void setOperationBackgroundImg(int operationBackgroundImg) {
		this.operationBackgroundImg = operationBackgroundImg;
	}
	

	public int getOperationBackgroundImg() {
		return operationBackgroundImg;
	}

	
	private int operationSignImg              			= 0;
	private int LearningCenterMathOperationId   		= 0;
	private int defaultLearningCenterOperation 			= 0;
	
	private int equationsId                     		= 0;
	private int number1                     		    = 0;
	private int number2                     		    = 0;
	private int product                     		    = 0;
	private String operator                		        = null;
	
	private String number1Str                     		= null;
	private String number2Str                     		= null;
	private String productStr                     		= null;
	private String category                             = null;
	
	private boolean isDefaultLearningCenterOperation 	= false;
	private boolean isCorrectIncorrect              	= false;
	
	//added
	private String mathOperationCategory                = null;
	private int mathOperationCategoryId                 = 0;
	
	
	public String getMathOperationCategory() {
		return mathOperationCategory;
	}

	public void setMathOperationCategory(String mathOperationCategory) {
		this.mathOperationCategory = mathOperationCategory;
	}

	public int getMathOperationCategoryId() {
		return mathOperationCategoryId;
	}

	public void setMathOperationCategoryId(int mathOperationCategoryId) {
		this.mathOperationCategoryId = mathOperationCategoryId;
	}
	public void setOperationSignImg(int operationSignImg) {
		this.operationSignImg = operationSignImg;
	}
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	

	
	public int getOperationSignImg() {
		return operationSignImg;
	}

	

	public int getLearningCenterMathOperationId() {
		return LearningCenterMathOperationId;
	}

	public void setLearningCenterMathOperationId(
			int learningCenterMathOperationId) {
		LearningCenterMathOperationId = learningCenterMathOperationId;
	}

	public int getDefaultLearningCenterOperation() {
		return defaultLearningCenterOperation;
	}

	public void setDefaultLearningCenterOperation(
			int defaultLearningCenterOperation) {
		this.defaultLearningCenterOperation = defaultLearningCenterOperation;
	}

	public boolean isDefaultLearningCenterOperation() {
		return isDefaultLearningCenterOperation;
	}

	public void setDefaultLearningCenterOperation(
			boolean isDefaultLearningCenterOperation) {
		this.isDefaultLearningCenterOperation = isDefaultLearningCenterOperation;
	}

	public int getEquationsId() {
		return equationsId;
	}

	public void setEquationsId(int equationsId) {
		this.equationsId = equationsId;
	}

	public int getNumber1() {
		return number1;
	}

	public void setNumber1(int number1) {
		this.number1 = number1;
	}

	public int getNumber2() {
		return number2;
	}

	public void setNumber2(int number2) {
		this.number2 = number2;
	}

	public int getProduct() {
		return product;
	}

	public void setProduct(int product) {
		this.product = product;
	}

	public String getOperator() {
		return operator;
	}

	public void setOperator(String operator) {
		this.operator = operator;
	}

	public boolean isCorrectIncorrect() {
		return isCorrectIncorrect;
	}

	public void setCorrectIncorrect(boolean isCorrectIncorrect) {
		this.isCorrectIncorrect = isCorrectIncorrect;
	}

	public String getNumber1Str() {
		return number1Str;
	}

	public void setNumber1Str(String number1Str) {
		this.number1Str = number1Str;
	}

	public String getNumber2Str() {
		return number2Str;
	}

	public void setNumber2Str(String number2Str) {
		this.number2Str = number2Str;
	}

	public String getProductStr() {
		return productStr;
	}

	public void setProductStr(String productStr) {
		this.productStr = productStr;
	}
}
