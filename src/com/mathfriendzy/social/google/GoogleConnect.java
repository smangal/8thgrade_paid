package com.mathfriendzy.social.google;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender.SendIntentException;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesClient.ConnectionCallbacks;
import com.google.android.gms.common.GooglePlayServicesClient.OnConnectionFailedListener;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

public class GoogleConnect implements ConnectionCallbacks, 
OnConnectionFailedListener,
GoogleApiClient.ConnectionCallbacks,
GoogleApiClient.OnConnectionFailedListener{

	/* Client used to interact with Google APIs. */
	private GoogleApiClient mGoogleApiClient;
	private Context context = null;

	/* A flag indicating that a PendingIntent is in progress and prevents
	 * us from starting further intents.
	 */
	private boolean mIntentInProgress;

	/* Track whether the sign-in button has been clicked so that we know to resolve
	 * all issues preventing sign-in without waiting.
	 */
	private boolean mSignInClicked;

	/* Store the connection result from onConnectionFailed callbacks so that we can
	 * resolve them when the user clicks sign-in.
	 */
	private ConnectionResult mConnectionResult;
	//private View loginButton = null;

	/* Request code used to invoke sign in user interactions. */
	private static final int RC_SIGN_IN = 0;

	//private Location mLastLocation = null;

	private ProgressDialog pd = null;
	public GoogleConnect(Context contex){
		this.context = contex;
	}

	/**
	 * Initialize the progress dialog which will show 
	 * at the time of login
	 * @param context
	 */
	private void initializeProcessDialog(Context context){
		pd = new ProgressDialog(context);
		pd.setIndeterminate(false);//back button does not work
		pd.setCancelable(false);
		pd.setMessage("Please Wait...");
	}

	/**
	 * Initialize the google client
	 */
	public void initializeGoogleClient(){
		// Initializing google plus api client
		mGoogleApiClient = new GoogleApiClient.Builder(context)
		.addConnectionCallbacks(this)
		.addOnConnectionFailedListener(this)
		.addApi(Plus.API)
		.addScope(Plus.SCOPE_PLUS_LOGIN).build();
		this.initializeProcessDialog(context);
	}

	/**
	 * Show the progress dialog
	 */
	private void showDialog(){
		if(pd != null)
			pd.show();
	}

	/**
	 * Dismiss progress dialog
	 */
	private void dismissDialog(){
		if(pd != null && pd.isShowing())
			pd.cancel();
	}

	/**
	 * Check for google client is connected or not
	 * @return
	 */
	public boolean isConnected(){
		return mGoogleApiClient.isConnected();
	}

	/**
	 * Check for isConneting
	 * @return
	 */
	public boolean isConnecting(){
		return mGoogleApiClient.isConnecting();
	}

	/**
	 * Sign in to Google
	 */
	public void signIn(){
		if (!mGoogleApiClient.isConnecting()) {
			mSignInClicked = true;
			resolveSignInError();
		}
	}

	/**
	 * Sign Out from Google
	 */
	public void signOut(){
		if (mGoogleApiClient.isConnected()) {
			Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
			mGoogleApiClient.disconnect();
			mGoogleApiClient.connect();
		}
	}

	/**
	 * Connet the google client
	 * On start call from the onStart() of the activity
	 */
	public void onStart(){
		mGoogleApiClient.connect();
	}

	/**
	 * Disconnet the google client
	 * on stop call from the onStop() of the activity
	 */
	public void onStop(){
		if (mGoogleApiClient.isConnected()) {
			mGoogleApiClient.disconnect();
		}
	}

	/**
	 * Method to resolve any signin errors
	 * */
	private void resolveSignInError() {
		if(mConnectionResult != null){
			if (mConnectionResult.hasResolution()) {
				try {
					mIntentInProgress = true;
					mConnectionResult.startResolutionForResult
					((Activity) context, RC_SIGN_IN);
				} catch (SendIntentException e) {
					mIntentInProgress = false;
					mGoogleApiClient.connect();
				}
				this.showDialog();
			}
		}
	}

	@Override
	public void onConnectionFailed(ConnectionResult result) {
		this.dismissDialog();
		if (!result.hasResolution()) {
			GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), 
					(Activity) context,
					0).show();
			return;
		}

		if (!mIntentInProgress) {
			// Store the ConnectionResult for later usage
			mConnectionResult = result;

			if (mSignInClicked) {
				// The user has already clicked 'sign-in' so we attempt to
				// resolve all
				// errors until the user is signed in, or they cancel.
				resolveSignInError();
			}
		}
	}

	@Override
	public void onConnected(Bundle arg0) {
		this.dismissDialog();
		mSignInClicked = false;
		this.profileData();
	}


	@Override
	public void onDisconnected() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConnectionSuspended(int arg0) {
		mGoogleApiClient.connect();
	}


	/**
	 * Call for getting profile data
	 */
	private void profileData(){
		try {
			if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
				Person currentPerson = Plus.PeopleApi
						.getCurrentPerson(mGoogleApiClient);
				String userId = currentPerson.getId();
				String personName = currentPerson.getDisplayName();
				String personPhotoUrl = currentPerson.getImage().getUrl();
				String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

				GoogleProfile profile = new GoogleProfile();
				profile.setUserId(userId);
				profile.setPersonName(personName);
				profile.setPersonPhotoUrl(personPhotoUrl);
				profile.setEmail(email);
				profile.setfName(currentPerson.getName().getGivenName());
				profile.setlName(currentPerson.getName().getFamilyName());
				try{
					//create user name from email
					profile.setUserName(email.split("@")[0]);
				}catch(Exception e){
					e.printStackTrace();
				}
				((MyGoogleCallBack) context).onProfileData(profile);
			} else {
				Toast.makeText(context.getApplicationContext(),
						"Person information is null", Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Work as a onActivity result of activity
	 * call from the onActivityResult of the activity
	 * @param requestCode
	 * @param responseCode
	 * @param intent
	 */
	public void onActivityResultCall(int requestCode, int responseCode,
			Intent intent){

		if (requestCode == RC_SIGN_IN) {
			if (responseCode != Activity.RESULT_OK) {
				mSignInClicked = false;
			}
			mIntentInProgress = false;
			if (!mGoogleApiClient.isConnecting()) {
				mGoogleApiClient.connect();
			}
		}
	}

	/*private void getLocation(){
	new Thread(new Runnable() {
		@Override
		public void run() {
			mLastLocation = LocationServices.FusedLocationApi.
					getLastLocation(mGoogleApiClient);
			getLocation();
			}
		}).start();
	}*/

	/*public void getAddress() {
    // Ensure that a Geocoder services is available
    if (Build.VERSION.SDK_INT >=
            Build.VERSION_CODES.GINGERBREAD
            &&
            Geocoder.isPresent()) {


	 * Reverse geocoding is long-running and synchronous.
	 * Run it on a background thread.
	 * Pass the current location to the background task.
	 * When the task finishes,
	 * onPostExecute() displays the address.

        (new GetAddressTask(context)).execute(mLastLocation);
    }
	}*/

	/*private class GetAddressTask extends AsyncTask<Location, Void, String>{

		Context mContext;

	    public GetAddressTask(Context context) {
	        super();
	        mContext = context;
	    }

	    @Override
	    protected String doInBackground(Location... params) {
	        Geocoder geocoder =
	                new Geocoder(mContext, Locale.getDefault());
	        // Get the current location from the input parameter list
	        Location loc = params[0];
	        // Create a list to contain the result address
	        List<Address> addresses = null;
	        try {
	            addresses = geocoder.getFromLocation(loc.getLatitude(),
	                    loc.getLongitude(), 1);
	        } catch (IOException e1) {
	            Log.e("LocationSampleActivity",
	                    "IO Exception in getFromLocation()");
	            e1.printStackTrace();
	            return ("IO Exception trying to get address");
	        } catch (IllegalArgumentException e2) {
	            // Error message to post in the log
	            String errorString = "Illegal arguments " +
	                    Double.toString(loc.getLatitude()) +
	                    " , " +
	                    Double.toString(loc.getLongitude()) +
	                    " passed to address service";
	            Log.e("LocationSampleActivity", errorString);
	            e2.printStackTrace();
	            return errorString;
	        }
	        // If the reverse geocode returned an address
	        if (addresses != null && addresses.size() > 0) {
	            // Get the first address
	            Address address = addresses.get(0);

	 * Format the first line of address (if available),
	 * city, and country name.

	            String addressText = String.format(
	                    "%s, %s, %s",
	                    // If there's a street address, add it
	                    address.getMaxAddressLineIndex() > 0 ?
	                            address.getAddressLine(0) : "",
	                    // Locality is usually a city
	                    address.getLocality(),
	                    // The country of the address
	                    address.getCountryName());
	            // Return the text
	            return addressText;
	        } else {
	            return "No address found";
	        }
	    }

	    @Override
	    protected void onPostExecute(String address) {
	        // Set activity indicator visibility to "gone"
	        // Display the results of the lookup.
	        Log.e("", "Address " + address);
	    }
	}*/
}
