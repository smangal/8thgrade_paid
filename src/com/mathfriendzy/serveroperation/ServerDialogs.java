package com.mathfriendzy.serveroperation;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;

public class ServerDialogs {
	
	/**
	 * This method return the progress dialog to be displayed
	 * @param context
	 * @param msg
	 * @param dialogCode
	 * @return
	 */
	public static ProgressDialog getProgressDialog(Context context , String msg , int dialogCode){
		ProgressDialog pd = new ProgressDialog(context);
		pd.setMessage(msg);
		pd.setIndeterminate(false);
		pd.setCancelable(false);
		return pd;
	}
	
	/**
	 * This method return the progress dialog to be displayed
	 * @param context
	 * @param msg
	 * @param dialogCode
	 * @return
	 */
	public static ProgressDialog getProgressDialogForWebView(Context context , String msg , int dialogCode){
		ProgressDialog pd = new ProgressDialog(context);
		pd.setMessage(msg);
		pd.setIndeterminate(false);
		pd.setCancelable(false);
		return pd;
	}
	
	/**
	 * @param context - activity object
	 * @param msg     - msg to print
	 */
	public static void warningDialog(Context context , String msg){
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		dialog.setIcon(android.R.drawable.ic_dialog_alert);
		dialog.setTitle("MakeMySearch!!");
		dialog.setMessage(msg);
		dialog.setPositiveButton("Ok", new OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				}
		});
		dialog.show();
	}
	
}
