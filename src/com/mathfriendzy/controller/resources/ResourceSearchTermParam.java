package com.mathfriendzy.controller.resources;

import java.io.Serializable;

/**
 * Created by root on 12/1/16.
 */
public class ResourceSearchTermParam implements Serializable{
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private String searchTerm;
    private boolean isDirectSearchFromCategoryScreen = false;
    private String selectedGrades;
    private int selectedResourceFormat;
    private String selectedSubject;
    private int selectedLang;

    public String getSearchTerm() {
        return searchTerm;
    }

    public void setSearchTerm(String searchTerm) {
        this.searchTerm = searchTerm;
    }

    public boolean isDirectSearchFromCategoryScreen() {
        return isDirectSearchFromCategoryScreen;
    }

    public void setDirectSearchFromCategoryScreen(
            boolean isDirectSearchFromCategoryScreen) {
        this.isDirectSearchFromCategoryScreen = isDirectSearchFromCategoryScreen;
    }

    public String getSelectedGrades() {
        return selectedGrades;
    }

    public void setSelectedGrades(String selectedGrades) {
        this.selectedGrades = selectedGrades;
    }

    public int getSelectedResourceFormat() {
        return selectedResourceFormat;
    }

    public void setSelectedResourceFormat(int selectedResourceFormat) {
        this.selectedResourceFormat = selectedResourceFormat;
    }

    public String getSelectedSubject() {
        return selectedSubject;
    }

    public void setSelectedSubject(String selectedSubject) {
        this.selectedSubject = selectedSubject;
    }

    public int getSelectedLang() {
        return selectedLang;
    }

    public void setSelectedLang(int selectedLang) {
        this.selectedLang = selectedLang;
    }
}
