package com.mathfriendzy.controller.moreapp;

import java.util.ArrayList;

public interface MoreAppListener {
	void onSuccess(ArrayList<AppDetail> appList);
}
