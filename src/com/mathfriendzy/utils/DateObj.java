package com.mathfriendzy.utils;

public class DateObj {
	
	public int getDiffInDay() {
		return diffInDay;
	}
	public void setDiffInDay(int diffInDay) {
		this.diffInDay = diffInDay;
	}
	public int getDiffInHours() {
		return diffInHours;
	}
	public void setDiffInHours(int diffInHours) {
		this.diffInHours = diffInHours;
	}
	public int getDiffInMin() {
		return diffInMin;
	}
	public void setDiffInMin(int diffInMin) {
		this.diffInMin = diffInMin;
	}
	public int getDiffInSec() {
		return diffInSec;
	}
	public void setDiffInSec(int diffInSec) {
		this.diffInSec = diffInSec;
	}
	private int diffInDay;
	private int diffInHours;
	private int diffInMin;
	private int diffInSec;
}
